import { Fragment, useState, useEffect } from 'react'
import ProductCard from "./ProductCard";


export default function UserView({productData}) {

    // console.log(coursesData);



    const [product, setProducts] = useState([]);

    useEffect(() => {
        
        // Map through the courses received from the parent component (courses page) to render the course cards
        const productsArr = productData.map(product => {
            // Returns active courses as "CourseCard" components
        	if(product.isActive === true){
				return (
					<ProductCard productProp={product} key={product._id}/>
				)
        	}else{
        		return null;
        	}
        },[]);

        // Set the "courses" state with the course card components returned by the map method
        // Allows the course card components to be rendered in this "UserView" component via the return statement below
        setProducts(productsArr);

    }, [productData]);

    
    return(
        <Fragment>
            {product}
        
        </Fragment>
    );
}
