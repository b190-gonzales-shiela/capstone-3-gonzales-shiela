import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function ProductView(){

	const { user }= useContext(UserContext);

	const navigate= useNavigate();

	const {productId}= useParams();

	const [name, setName]= useState('');
	const [description, setDescription]= useState('');
	const [price, setPrice]= useState(0);

	const [order, setOrder]= useState("");

	const product=(productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/products`,{
			method: "POST",
			headers:{
				'Content-Type':'application/json',
				 Authorization:`Bearer${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data=>{
			console.log(data);

			if (data===true){
				Swal.fire({
					title: "Successfully Added",
					icon: "success",
					text: "Successfully Checked Out! Details will be sent via email"
				})

				navigate('/products');
			}else {
				Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text: "try again"
			})

			}
			
		})
	}

	const checkout=(productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/checkout/${productId}`,{
			method: "POST",
			headers:{
				'Content-Type':'application/json',
				 Authorization:`Bearer${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data=>{
			console.log(data);

			if (data!==false){
				Swal.fire({
					title: "Successfully Added",
					icon: "success",
					text: "Successfully Added to Cart!"
				})

				navigate('/products');
			}else {
				Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text: "try again"
			})

			}
			
		})
	}


	useEffect(()=>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res=> res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	},[productId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body className="text-center">

							<Card.Title> {name} </Card.Title>

							<Card.Title> Description </Card.Title>

							<Card.Subtitle> {description} </Card.Subtitle>

							<Card.Title> Price </Card.Title>
							<Card.Subtitle> {price} </Card.Subtitle>

							
							<Card.Title> Store Schedule </Card.Title>
							<Card.Subtitle> 8am-5pm </Card.Subtitle>
							{(user.id !==null)?
								<Button variant="primary" onClick={()=>checkout(productId)} block>Buy</Button>
								:
								<Link className='btn btn-danger btn-block' to='/login'> Login to shop </Link>

							}

							
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>




		)
}
