import Card from 'react-bootstrap/Card';
import CardGroup from 'react-bootstrap/CardGroup';
import Button from 'react-bootstrap/Button';
import {Link} from 'react-router-dom';

function GroupExample() {
  return (
    <CardGroup>
      <Card>
        <Card.Img variant="top" src={require('../images/teambride.jpeg')} />
        <Card.Body>
          <Card.Title>Theme Bride Fondant Cupcakes 12s</Card.Title>
          <Card.Subtitle className="mb-2 text-danger">P1500</Card.Subtitle>
          <Card.Text>
           12s Team bride Fondant Cupcakes
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <Link to="./Products">
        <Button variant="link">Products</Button>
          </Link>
        </Card.Footer>

      </Card>
      <Card>
        <Card.Img variant="top" src={require('../images/marvel.jpeg')} />
        <Card.Body>
          <Card.Title>Marvel themed fondant detailed cake</Card.Title>
          <Card.Subtitle className="mb-2 text-danger">P1500</Card.Subtitle>
          <Card.Text>
            7x5 Marvel themed fondant detailed cake. {' '}
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <Link to="./Products">
        <Button variant="link">Products</Button>
          </Link>
        </Card.Footer>

      </Card>
      <Card>
        <Card.Img variant="top" src={require('../images/rainbow cake.jpeg')} />
        <Card.Body>
          <Card.Title>Rainbow Rollerskates Cake</Card.Title>
          <Card.Subtitle className="mb-2 text-danger">P1500</Card.Subtitle>
          <Card.Text>
           7x5 RollerSkates Rainbow Themed Cake.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <Link to="./Products">
        <Button variant="link">Products</Button>
          </Link>
        </Card.Footer>

      </Card>
       <Card>
        <Card.Img variant="top" src={require('../images/edible print baby shark.JPG')} />
        <Card.Body>
        
        
          <Card.Title>Baby Shark Number Cake</Card.Title>
          <Card.Subtitle className="mb-2 text-danger">P1500</Card.Subtitle>
          <Card.Text>
           Baby Shark Themed Cake
          </Card.Text>
        </Card.Body>
        
        <Card.Footer>
          <Link to="./Products">
        <Button variant="link">Products</Button>
          </Link>
        </Card.Footer>

      </Card>
    </CardGroup>

  );
  
}

export default GroupExample;