import React from "react";
import {
  Box,
  Container,
  Row,
  Column,
  FooterLink,
  Heading,
} from "./FooterStyles";
  
const Footer = () => {
  return (
    <Box>
      <h3 style={{ color: "black", 
                   textAlign: "center", 
                   marginTop: "20px" }}>
      </h3>
      <Container>
        <Row>
          For educational purposes only. September 2022.
        </Row>
      </Container>
    </Box>
  );
};
export default Footer;