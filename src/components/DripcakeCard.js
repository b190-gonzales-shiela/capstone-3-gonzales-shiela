import Card from 'react-bootstrap/Card';
import CardGroup from 'react-bootstrap/CardGroup';
import Button from 'react-bootstrap/Button';
import {Link} from 'react-router-dom';

export default function GroupExample() {
  return (
    <CardGroup>
      <Card>
        <Card.Img variant="top" src={require('../images/dripcakeblue.jpeg')} />
        <Card.Body>
          <Card.Title>Chocolate Overload Drip Cake</Card.Title>
          <Card.Subtitle className="mb-2 text-danger">P1500</Card.Subtitle>
          <Card.Text>
            7x5 Blippi All Edible Fondant Cake
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <Link to="./Products">
        <Button variant="link">Products</Button>
          </Link>
        </Card.Footer>

      </Card>
      <Card>
        <Card.Img variant="top" src={require('../images/Dripblack.jpeg')} />
        <Card.Body>
          <Card.Title>Liquor Themed Drip Cake</Card.Title>
          <Card.Subtitle className="mb-2 text-danger">P1500</Card.Subtitle>
          <Card.Text>
            7x5 Spiderman themed fondant detailed cake. {' '}
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <Link to="./Products">
        <Button variant="link">Products</Button>
          </Link>
        </Card.Footer>
       
      </Card>
      <Card>
        <Card.Img variant="top" src={require('../images/elsa.jpeg')} />
        <Card.Body>
          <Card.Title>Frozen Fondant Drip Cake</Card.Title>
          <Card.Subtitle className="mb-2 text-danger">P1500</Card.Subtitle>
          <Card.Text>
           7x5 Lightning Mcqueen full fondant cake with toy topper.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <Link to="./Products">
        <Button variant="link">Products</Button>
          </Link>
        </Card.Footer>

      </Card>
       <Card>
        <Card.Img variant="top" src={require('../images/drip cake with chocolate.jpeg')} />
        <Card.Body>
          <Card.Title>Chocolate Overload Drip Cake</Card.Title>
          <Card.Subtitle className="mb-2 text-danger">P1500</Card.Subtitle>
          <Card.Text>
           7x5 Godzilla full fondant cake with toy topper.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <Link to="./Products">
        <Button variant="link">Products</Button>
          </Link>
        </Card.Footer>

      </Card>
    </CardGroup>

  );
  
}
