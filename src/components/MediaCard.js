import Card from 'react-bootstrap/Card';
import CardGroup from 'react-bootstrap/CardGroup';
import Button from 'react-bootstrap/Button';
import {Link} from 'react-router-dom';


function GroupExample() {
  return (
    <CardGroup>
      <Card>
        <Card.Img variant="top" src={require('../images/blippi.JPG')} />
        <Card.Body>
          <Card.Title>Blippi themed Full Fondant Cake</Card.Title>
          <Card.Subtitle className="mb-2 text-danger">P1500</Card.Subtitle>
          <Card.Text>
            7x5 Blippi All Edible Fondant Cake
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <Link to="./Products">
        <Button variant="link">Products</Button>
          </Link>
        </Card.Footer>
      </Card>
      <Card>
        <Card.Img variant="top" src={require('../images/character-spiderman cake.jpeg')} />
        <Card.Body>
          <Card.Title>Spiderman themed fondant detailed cake</Card.Title>
          <Card.Subtitle className="mb-2 text-danger">P1500</Card.Subtitle>
          <Card.Text>
            7x5 Spiderman themed fondant detailed cake. 
          </Card.Text>
        </Card.Body>
        <Card.Footer>
            <Link to="./Products">
          <Button variant="link">Products</Button>
            </Link>
        </Card.Footer>
      </Card>
      <Card>
        <Card.Img variant="top" src={require('../images/lightningmcqueencake.JPG')} />
        <Card.Body>
          <Card.Title>Lightning Mcqueen Full Fondant Cake</Card.Title>
          <Card.Subtitle className="mb-2 text-danger">P1500</Card.Subtitle>
          <Card.Text>
           7x5 Lightning Mcqueen full fondant cake with toy topper.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
            <Link to="./Products">
          <Button variant="link">Products</Button>
            </Link>
        </Card.Footer>
      </Card>
       <Card>
        <Card.Img variant="top" src={require('../images/godzilla.jpeg')} />
        <Card.Body>
          <Card.Title>Godzilla Full Fondant Cake</Card.Title>
          <Card.Subtitle className="mb-2 text-danger">P1500</Card.Subtitle>
          <Card.Text>
           7x5 Godzilla full fondant cake with toy topper.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
            <Link to="./Products">
          <Button variant="link">Products</Button>
            </Link>
        </Card.Footer>
      </Card>
    </CardGroup>

  );
  
}

export default GroupExample;