import { Fragment, useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){

	console.log(props);
	// Destructure our courses data from the props being passed by the parent component (courses page)
	// Includes the "fetchData" function that retrieves the courses from our database
	const { productData, fetchData } = props;
	const {orderData,fetchOrderData}= props;


	// States for form inputs
	const [productId, setProductId] = useState("");
	const [products, setProducts] = useState([]);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [userName, setUserName]=useState(""); 
	const [userId, setUserId]= useState("");
	const [isAdmin, setIsAdmin]= useState("");
	const [quantity, setQuantity]=useState("");
	const [isStatus, setStatus]=useState("");
	const [mobileNo, setMobileNo]= useState("");
	const [message, setMessage]=useState("");
	const [orders, setOrders]=useState("");
	const [orderId, setOrderId]=useState("");
	const [isCompleted, setIsCompleted]=useState("");
	const [searchParam]= useState(['orderId','productId']);


	// States to open/close modals
	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);
	const [showOrder, setShowOrder]= useState(false);
	const [openAdmin, setShowAdmin]= useState(false);




	// Functions to toggle the opening and closing of the "Add Course" modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	// Add-Admin open modal

	const makeAdmin = () => setShowAdmin(true);
	const closeAdmin = () => setShowAdmin(false);


	// Get One Order

	const oneOrder=()=>setShowOrder(true);
	const closeOrder=()=>setShowOrder(false);






	/* 
	Function to open the "Edit Course" modal:
		- Fetches the selected course data using the course ID
		- Populates the values of the input fields in the modal form
		- Opens the "Edit Course" modal
	*/
	const openEdit = (productId) => {

		// Fetches the selected course data using the course ID
		fetch(`${ process.env.REACT_APP_API_URL}/products/${ productId }`)
		.then(res => res.json())
		.then(data => {

			// console.log(data);

			// Changes the states for binded to the input fields
			// Populates the values of the input files in the modal form
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

		// Opens the "Edit Course" modal
		setShowEdit(true);
	};

	/* 
	Function to close our "Edit Course" modal:
		- Reset from states back to their initial values
		- Empties the input fields in the form whenever the modal is opened for adding a course
	*/
	const closeEdit = () => {

		setShowEdit(false);
		setName("");
		setDescription("");
		setPrice(0);

	};

	const addProduct = (e) => {

		// Prevent the form from redirecting to a different page on submit 
		// Helps retain the data if adding a course is unsuccessful
		e.preventDefault()

		fetch(`${ process.env.REACT_APP_API_URL }/products/addProduct`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			// If the new course is successfully added
			if (data === true) {

				// Invoke the "fetchData" function passed from our parent component (courses page)
				// Rerenders the page because of the "useEffect"
				fetchData();

				// Show a success message via sweet alert
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully added."					
				})

				// Reset all states to their initial values
				// Provides better user experience by clearing all the input fieles when the user adds another course
				setName("")
				setDescription("")
				setPrice(0)

				// Close the modal
				closeAdd();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const editProduct = (e, productId) => {
		
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL }/products/${ productId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data !== false) {

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully updated."
				});

				closeEdit();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		})
	}
// ARCHIVING PRODUCT
	useEffect(() => {


		const archiveProduct= (productId, isActive) => {

			console.log(!isActive);

			fetch(`${ process.env.REACT_APP_API_URL }/products/archive/${ productId }`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(res => res.json())
			.then(data => {

				if (data === true) {

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully archived/unarchived."
					});

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}

		const productArr = productData.map(product => {

			return(

				<tr key={product._id}>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>
						{/* 
							- If the course's "isActive" field is "true" displays "available"
							- Else if the course's "isActive" field is "false" displays "unavailable"
						*/}
						{product.isActive
							? <span>Available</span>
							: <span>Unavailable</span>
						}
					</td>
					<td>
						<Button
							variant="outline-primary"
							size="sm"
							onClick={() => openEdit(product._id)}
						>
							Update
						</Button>
						{/* 
							- Display a red "Disable" button if course is "active"
							- Else display a green "Enable" button if course is "inactive"
						*/}
						{product.isActive
							?
							<Button 
								variant="outline-danger" 
								size="sm" 
								onClick={() => archiveProduct(product._id, product.isActive)}
							>
								Disable
							</Button>
							:
							<Button 
								variant="outline-success"
								size="sm"
								onClick={() => archiveProduct(product._id, product.isActive)}
							>
								Enable
							</Button>


						}
					</td>
				</tr>

			)

		});

		
		setProducts(productArr);

	}, [productData, fetchData]);

// ========================================================================================

		// ADDING/MAKING ANOTHER ADMIN


		const addAdmin = (e) => {

			// Prevent the form from redirecting to a different page on submit 
			// Helps retain the data if adding a course is unsuccessful
			e.preventDefault()

			fetch(`${ process.env.REACT_APP_API_URL }/users/admin/${ userId }`, {
				method: 'PUT',
				headers: {
					Authorization: `Bearer ${ localStorage.getItem('token') }`,
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					userName: userName,
					userId: userId,
					isAdmin: true
				})
			})
			.then(res => res.json())
			.then(data => {

				// If the new course is successfully added
				if (data === true) {

					// Invoke the "fetchData" function passed from our parent component (courses page)
					// Rerenders the page because of the "useEffect"
					fetchData();

					// Show a success message via sweet alert
					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Successfully added new admin"					
					})

					// Reset all states to their initial values
					// Provides better user experience by clearing all the input fieles when the user adds another course
					setUserName("")
					setUserId("")
					setIsAdmin(true)

					// Close the modal
					closeAdmin();

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
				}
			})
		}

// ========================================================================================


						// RETRIEVING ONE ORDER
				useEffect(() => {

						const oneOrder = (e) => {

							// Prevent the form from redirecting to a different page on submit 
							// Helps retain the data if adding a course is unsuccessful
							e.preventDefault()

							fetch(`${ process.env.REACT_APP_API_URL }/orders/${orderId}`, {
								method: 'GET',
								headers: {
									Authorization: `Bearer ${ localStorage.getItem('token') }`,
									'Content-Type': 'application/json'
								},
								body: JSON.stringify({
									userName: userName,
									userId: userId,
									mobileNo: mobileNo,
									productId: productId,
									quantity: quantity,
									message: message
								})
							})
							.findOne(res => res.json())
							.then(data => {

								// If the new course is successfully added
								if (data === true) {

									// Invoke the "fetchData" function passed from our parent component (courses page)
									// Rerenders the page because of the "useEffect"
									fetchData();

									// Show a success message via sweet alert
									Swal.fire({
										title: "Success",
										icon: "success",
										text: "Successfully added new admin"					
									})

									// Reset all states to their initial values
									// Provides better user experience by clearing all the input fieles when the user adds another course


									// Close the modal
									closeOrder();

								} else {

									fetchData();

									Swal.fire({
										title: "Something went wrong",
										icon: "error",
										text: "Please try again."
									})
								}
							})
						}
					});


	return(
		<Fragment>

			<div className="text-center my-4">
				<h2>Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button variant="light" size="sm"onClick={openAdd}>Add New Product</Button>	
					<Button variant="light" size="sm" onClick={makeAdmin}>Make User Admin</Button>
				
				</div>	
				
			
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>					
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>

			{/*ADD MODAL*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			<Modal show={openAdmin} onHide={closeAdmin}>
				<Form onSubmit={e => addAdmin(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Make Admin</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="userName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>User Id</Form.Label>
							<Form.Control type="text" value={userId}  onChange={e => setUserId(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="isAdmin">
							<Form.Label>is Admin?</Form.Label>
							<Form.Control type="boolean" value={isAdmin}  onChange={e => setIsAdmin(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdmin}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			{/*EDIT MODAL*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			<Modal show={showOrder} onHide={closeOrder}>
				<Form onSubmit={findOne => oneOrder(orderId)}>
					<Modal.Header closeButton>
						<Modal.Title>Fetch One Order</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productName">
							<Form.Label>Order ID</Form.Label>
							<Form.Control type="text" value={orderId} onChange={e => setOrderId(e.target.value)} required/>
		
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeOrder}>Close</Button>
						<Button variant="success" type="search">Find</Button>
					</Modal.Footer>
				</Form>
			</Modal>


			
			
		</Fragment>
	)
}
