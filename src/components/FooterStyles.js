import styled from 'styled-components';
   
export const Box = styled.div`
  padding: 5px 10px;
  background: lightpink;
  position: bottom;
  bottom: 0;
  width: 100%;
  margin-top: 90px;
  
   
  @media (max-width: 1000px) {
    padding: 70px 30px;
  }
`;
   
export const Container = styled.div`
    display: flex;
  
    justify-content: center;
    max-width: 1000px;
    margin: 0 auto;
    height: '60px',
    lineHeight: '60px'
    /* background: red; */
  
`;
   
export const Row = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, 
                         minmax(185px, 1fr));
  grid-gap: 20px;
   
  @media (max-width: 500px) {
    grid-template-columns: repeat(auto-fill, 
                           minmax(200px, 1fr));
  }
`;
   
export const FooterLink = styled.a`
  color: #fff;
  margin-bottom: 5px;
  font-size: 12px;
  text-decoration: none;
   
  &:hover {
      color: black;
      transition: 200ms ease-in;
  }
`;
   
export const Heading = styled.p`
  font-size: 8px;
  color: #fff;
  margin-bottom: 5px;
  font-weight: bold;
`;