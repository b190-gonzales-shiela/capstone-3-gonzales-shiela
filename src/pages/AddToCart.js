import UserContext from '../UserContext';
import { useEffect , useState, Fragment } from'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';



 export default function AddToCart() {


	const [checkOut, setCheckOut]= useState([]);
    const [userId, setUserId]= useState("");
    const [userName, setUserName]= useState("");
    const [mobileNo, setMobileNo]=useState("");
    const [productId, setProductId]=useState("");
    const [quantity, setQuantity]=useState([]);
    const [count, setCount]=useState(1);
    const [item, setItem]=useState([]);


    // States to open/close modals
    const [showCheckOut, setShowCheckOut] = useState(false);

    // Functions to toggle the opening and closing of the "Add Course" modal
    const openCheckOut = () => setShowCheckOut(true);
    const closeCheckOut = () => setShowCheckOut(false);

    useEffect(() => {

    const addToCart = (e) => {

        // Prevent the form from redirecting to a different page on submit 
        // Helps retain the data if adding a course is unsuccessful
        e.preventDefault()

        fetch(`${ process.env.REACT_APP_API_URL }/orders/checkout/productId`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userName: userName,
                mobileNo: mobileNo,
                productId: productId,
                quantity: quantity
            })
        })
        .then(res => res.json())
        .then(data => {

            // If the new course is successfully added
            if (data === true) {

                // Invoke the "fetchData" function passed from our parent component (courses page)
                // Rerenders the page because of the "useEffect"
                fetch();

                // Show a success message via sweet alert
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Product successfully added to cart."                 
                })

                // Reset all states to their initial values
                // Provides better user experience by clearing all the input fieles when the user adds another course
               

                // Close the modal
                closeCheckOut();

            } else {

                fetch();

                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                })
            }
        })

    } 
	}, [checkOut]);

    



 return (
        <Fragment>
          
        <Modal show={showCheckOut} onHide={closeCheckOut}>
            <Form onSubmit={e => checkOut(e)}>
                <Modal.Header closeButton>
                    <Modal.Title>Check Out</Modal.Title>
                </Modal.Header>
                <Modal.Body>    
                    <Form.Group controlId="userName">
                        <Form.Label>userName</Form.Label>
                        <Form.Control type="text" value={userName} onChange={e => setUserName(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group controlId="mobileNo">
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" value={mobileNo}  onChange={e => setMobileNo(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group controlId="quantity">
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="number" value={quantity}  onChange={e => setQuantity(e.target.value)} required/>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={closeCheckOut}>Close</Button>
                    <Button variant="success" type="submit">Submit</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    </Fragment>
    )


}
