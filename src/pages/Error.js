import { Navigate } from 'react-router-dom';

export default function Error() {
	localStorage.clear();

	return(
		<Navigate to='/Banner' />
	)
}
