import { Fragment } from 'react';


import Banner from '../components/Banner';

import Carousel from '../components/Carousel';
import MediaCard from '../components/MediaCard';
import Separator from '../components/Separator';
import DripcakeCard from '../components/DripcakeCard';
import CupcakeMedia from '../components/CupcakeMedia';
import Footer from '../components/Footer';


export default function Home(){
	const data = {
		destination: "/products",
		label: "Shop Now!"
	}


	return(
		<Fragment>
			
			<Carousel />
			<Banner data={data} />
			<Separator />
			<MediaCard />
			<DripcakeCard />
			<CupcakeMedia />
			<Footer />
			


		</Fragment>
	)
}